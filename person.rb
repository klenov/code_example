class Person < ActiveRecord::Base
  acts_as_taggable
  attr_protected
  mount_uploader :person_pic, PersonPicUploader
  include ArchiveSanitize 
  include PgSearch
  extend FriendlyId
  include Twitter::Autolink
  include ValidationLogger
  include SaveEditor
  include Audits
  TAGS_CONTEXTS = %i( skills interests passions )
  TAGS_LOCALES  = %i( en ru )

  DISCIPLINES = %w(economics sociology politics architecture culture)
  SUBJECTS    = %w(researcher director tutor lecturer)

  friendly_id :name_en, use: :slugged

  multisearchable against: [:name_en, :name_ru, :cv_en, :cv_ru, :hashtags, :misc_search_text]

  has_one :user

  has_many :participations, dependent: :destroy, inverse_of: :person
  has_many :staffs,         dependent: :destroy, inverse_of: :person
  has_many :projects, through: :participations
  has_many :studios,  through: :staffs
  has_many :experiences, -> { order :created_at }
  has_many :places,  through: :experiences

  accepts_nested_attributes_for :experiences, allow_destroy: true

  before_validation do
    self.name_en = ArchiveSanitize.sanitize_and_strip self.name_en
    self.name_ru = ArchiveSanitize.sanitize_and_strip self.name_ru
    sanitize_cv
    save_hashtags
    downcase_string_columns
    save_misc_search_text
  end

  validates_uniqueness_of :name_en, :name_ru, {allow_nil: true, case_sensitive: false, allow_blank: true}
  validates_presence_of   :name_en
  validates_presence_of   :slug

  validates :discipline, allow_nil: true, inclusion: { in: DISCIPLINES }

  scope :find_by_email, -> email { joins(:user).where('users.email = ?', email) }
  scope :with_email,    -> { joins(:user).where('users.email is not null') }
  scope :rand_order,    -> { order('rand_order_number desc, id') }

  scope :discipline, -> discipline { where(discipline: discipline.downcase) }
  scope :visible,    -> { where(visible: true) }

  def self.random_reorder
    people = Person.order('RANDOM()').select(:id, :rand_order_number)

    people.each_with_index do |person, index| 
      person.update_column(:rand_order_number, index)
    end
  end

  def self.all_tags_keys
    arr = []
    TAGS_CONTEXTS.each{ |context| TAGS_LOCALES.each{ |locale| arr << "#{context}_#{locale}".to_sym } }
    arr
  end

  def final_project
    self.projects.public.limit(1).first
  end

  def projects_count
    self.projects.count 
  end

  def email
    if self.user.present?
      self.user.email
    end
  end

  def staff_role_title
    main_staff_role = self.staffs.select('role_id, count(role_id) as role_count')
                          .group(:role_id).order('role_count desc').first.try(:role_id)
    Role.staff.where(id: main_staff_role).pluck(:title_en).first if main_staff_role
  end

  def research_role_title
    main_research_role = self.participations.select('role_id, count(role_id) as role_count')
                             .group(:role_id).order('role_count desc').first.try(:role_id)
    Role.participation.where(id: main_research_role).pluck(:title_en).first if main_research_role
  end

  def researcher_most_common_studio
    studio_id = self.projects.where('studio_id is not null')
                    .select('studio_id, count(studio_id) as studio_count')
                    .group(:studio_id).order('studio_count desc').first.try(:studio_id)
    Studio.find(studio_id) if studio_id && studio_id > 0
  end

  def calculated_subject
    staff_role_title || research_role_title || 'Lecturer'
  end

  all_tags_keys.each do |key|
      define_method(key) do
        tags_in_context( key )
      end      
  end

  def tags_in_context( key )
    self.tags.in_context(key).order(:name).pluck(:name)
  end

  def connect_with_user(email)
    email = ArchiveSanitize.sanitize_and_strip(email).downcase
    user_by_email = User.find_by(email: email)

    if self.user.blank? # person не привязан к user
      user = User.where(email: email).first_or_create
      user.person_id = self.id
      user.save
    elsif self.user.email != email # person уже привязан к user и имейлы не совпадают (смена имейла)
      if user_by_email && user_by_email.guest? # удаляем guest юзера с таким имейлом, если он есть
        user_by_email.destroy
      end

      self.user.email = email
      self.user.save
    end 

    if self.reload.user.present? && self.user.valid? && self.user.guest?
      self.user.update_role('user')
      UserMailer.guest_was_approved(email).deliver
    end  
  end

  def pic_url
    if person_pic.present?
      person_pic.big.try(:url) if person_pic.big
    end
  end

  def normalize_friendly_id(text)
    text = Russian::Transliteration.transliterate(text) if text
    super( text )
  end

  private

  def save_hashtags
    hashtags_array = Twitter::Extractor.extract_hashtags( self.cv_en )
    self.hashtags =  hashtags_array.map{ |h| Digest::MD5.hexdigest h }.join(' ')
  end
  
  def downcase_string_columns
    self.discipline = discipline.downcase if discipline.present?
  end

  def save_misc_search_text
    self.misc_search_text = 
      ( self.tags.pluck(:name) +
        self.experiences.pluck(:subject_en) +
        self.experiences.pluck(:subject_ru) +
        self.places.pluck(:title_en)
      ).join(', ')
  end
end
