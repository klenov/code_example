module Audits
  attr_accessor :forced_audit_creation


  def self.included(base)
    base.extend(ClassMethods)
    base.send :include, InstanceMethods
    base.send :has_many, :audits, as: :auditable
    base.send :has_many, :connected_audits, as: :main_entity
    base.send :after_commit,    :create_audit
  end

  module ClassMethods
  end

  module InstanceMethods
    def last_stored_audit
      self.audits.order(id: :desc).first
    end

    def audit_hash
      self.to_json
    end

    def create_audit
      if self.previous_changes.any? || self.destroyed? || forced_audit_creation
        begin
          Audit.create_audit self, {action: audit_action, editor: self.updated_by}
        rescue => e
          ExceptionNotifier.notify_exception(e)
        end
      end
    end

    def main_audit_entity
      object_class = self.class.name.to_sym
      if Audit::AUDIT_MAIN_ENTITY[object_class] == :self
        self
      else
        self.send( Audit::AUDIT_MAIN_ENTITY[object_class] )
      end
    end

    def audit_action
      if self.destroyed?
        'delete'
      elsif self.transaction_record_state(:new_record) || !self.audits.any?
        'create'
      else
        'update'
      end
    end

  end
end