class CreateAudits < ActiveRecord::Migration
  def change
    create_table :audits do |t|
      t.belongs_to :auditable, polymorphic: true
      t.belongs_to :user
      t.text :state, null: false
      t.text :initial_state, null: false
      t.text :modifications, null: false
      t.string :action
      t.boolean :sended, null: false, default: false
      t.timestamps
    end

    add_index :audits, [:auditable_id, :auditable_type], name: 'auditable_index'
    add_index :audits, :user_id
    add_index :audits, [:sended, :auditable_type]
  end
end


