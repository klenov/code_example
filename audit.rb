class Audit < ActiveRecord::Base

    AUDIT_MAIN_ENTITY = 
    { 
      Project: :self, 
      Person:  :self,

      Pfile:         :project,
      Embed:         :project,
      Participation: :project,

      Experience: :person
    }

    class << self
      attr_accessor :current_editor
    end

    FILTERED_COLUMNS = %i( updated_at cv_html_en cv_html_ru desc_html_en desc_html_ru misc_search_text updated_by_id hashtags )

    belongs_to :user
    belongs_to :auditable,   polymorphic: true
    belongs_to :main_entity, polymorphic: true

    validates_presence_of :auditable, :user, :state, :modifications, :action

    scope :by_type,   -> type   { where(auditable_type: type) }
    scope :by_action, -> action { where(action: action) }
    scope :to_send,   -> { where(sended: false) }
    scope :to_send_by_type, -> type { to_send.by_type(type) }

    before_validation :serialize_state, on: :create

    def self.main_models
      AUDIT_MAIN_ENTITY.select{|k,v| v == :self }.keys
    end

    def self.create_audit(object, options)
      audit = self.new
      audit.auditable = object
      audit.main_entity = object.main_audit_entity
      audit.user   = Audit.current_editor || options[:editor]
      audit.action = options[:action]
      audit.state = object.audit_hash
      audit.modifications = object.previous_changes.to_json
      audit.save!
    end
    
    def self.send_audits_email
      recipients = User.where(subscribed_to_audits: true).pluck(:email)

      project_audits = find_project_audits_by_project_id
      people_audits  = find_people_audits_by_person_id

      if recipients.any? && ( project_audits.any? || people_audits.any? )
        UserMailer.audits_mail(recipients, project_audits, people_audits ).deliver
      end
    end

    def self.find_project_audits_by_project_id
      project_audits_by_project_id = {}

      recently_edited_projects = Project.where('updated_at > ?', 2.weeks.ago)
                                        .public.order(:updated_at)

      recently_edited_projects.each do |project|
        audits_ids = project.audits.to_send

        if audits_ids.any?
          project_audits_by_project_id[project.id] = audits_ids
        end
      end

      project_audits_by_project_id
    end

    def self.find_people_audits_by_person_id
      people_audits_by_person_id   = {}

      recently_edited_people = Person.where('updated_at > ?', 2.weeks.ago)
                                        .visible.order(:updated_at)

      recently_edited_people.each do |person|
        audits_ids = person.audits.to_send.order(:created_at)

        if audits_ids.any?
          people_audits_by_person_id[person.id] = audits_ids
        end
      end

      people_audits_by_person_id
    end

    def saved_audit_hash
      JSON.parse(self.state, symbolize_names: true)
    end

    def saved_modifications
      JSON.parse(self.modifications, symbolize_names: true)
    end

    def filtered_modifications
      saved_modifications.except( *FILTERED_COLUMNS )
    end

  private

  def serialize_state
    self.state = self.state.to_json
  end
end
