class PeopleController < ApplicationController
  load_and_authorize_resource
  skip_load_resource only: [:new, :create]
  before_action :filter_params, only: [:update, :create]
  before_action :set_tags_variables, only: [:update, :create]

  PER_PAGE = 20

  has_scope :discipline

  def index
    scope = apply_scopes(Person).visible

    @count  = scope.count
    @available_filters = available_filters(scope)
    @people = scope.order('people.updated_at desc').page(params[:page]).per(PER_PAGE)

    respond_to do |format|
      format.html
      format.json do
        render json: wrap_hash
      end
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json { render json: @person, serializer: PersonShowSerializer  }
    end
  end

  def create
    @person = Person.new( person_params.merge( editor_hash(true) ) )

    if @person.valid?
      set_tags
    end
    response_to_create_or_update_object(@person)
  end

  def update
    @person = Person.update( params[:id], person_params.merge( editor_hash ) )

    if @person.valid?
      set_tags
      connect_with_user
    end
    
    response_to_create_or_update_object(@person)
  end

  def edit
  end

  def new
  end

  def person_pic
    begin
      @person.person_pic = params[:file]
      @person.save
    rescue AWS::S3::Errors::RequestTimeout
      retry
    end  
    response_to_create_or_update_object(@person)
  end

  private

  def wrap_hash
    people_hash = @people.map do |person|
      PersonSerializer.new(person).serializable_hash
    end

    {items: people_hash}.merge( meta: meta_hash(@people, PER_PAGE, @count), available_filters: @available_filters )
  end

  def filter_params
    if params[:person_show]
      if params[:person_show][:experiences_attributes]
        params[:person_show][:experiences_attributes].each do |exp|
          exp[:place_id] = Place.where('LOWER( title_en ) = LOWER(?)', exp[:place]).first.try(:id)
          if !exp[:place_id].present?
            exp[:place_id] = Place.create(title_en: exp[:place].try(:downcase) ).id
          end
        end
      end
      
      if params[:person_show][:discipline] == ""
        params[:person_show][:discipline] = nil
      end
      if params[:person_show][:subject] == ''
        params[:person_show][:subject] = nil
      end
    end
    filter_tags_params
  end

  def filter_tags_params    
    Person.all_tags_keys.each do |tag_type|
      if params[:person_show].present? && params[:person_show].has_key?(tag_type) && params[:person_show][tag_type].nil?
        arams[:person_show][tag_type] = []
      end
    end
  end

  def set_tags_variables
    Person.all_tags_keys.each do |key|
      context_id = EasyTag::TagContext.where(name: key).first.id
      instance_variable_set( "@#{key}".to_sym, get_unique_tags(context_id) )
    end
  end

  def set_tags
    Person.all_tags_keys.each do |key|
      @person.set_tags( ext_person_params[key], context: key ) if ext_person_params[key]
    end
  end

  def connect_with_user
    @person.connect_with_user(ext_person_params[:email]) if ext_person_params[:email]
  end

  def get_unique_tags(context_id)
    EasyTag::Tagging.group(:tag_id, :name).joins(:tag).where(tag_context_id: context_id).pluck(:name)
  end

  def ext_person_params
    params.require(:person_show).permit(:email, *( Person::all_tags_keys.map{|key| { key => [] } } ) )
  end

  def person_params
    permit =  [:name_en, :name_ru, :cv_ru, :cv_en, :discipline, :remove_person_pic, :person_pic_desc, {experiences_attributes: [:id, :exp_type, :period, :subject_en, :subject_ru, :place_id, :_destroy]}]
    permit += [:visible] if can? :change_visibility, @person
    params.require(:person_show).permit(permit)
  end
end
